#!/usr/bin/env python

import sys
import os
import glob

sublist = glob.glob("*.substitutions")



for sub in sublist:
    prefix =  sub.split(".")[0]
    print ("Creating %s.db...." % (prefix))
    os.system("msi -S %s -I ../templ -o ../newdb/%s.db" % (sub,prefix))
