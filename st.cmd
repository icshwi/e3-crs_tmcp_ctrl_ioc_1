require essioc

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet(EPICS_DB_INCLUDE_PATH, "$(E3_CMD_TOP)/db:$(EPICS_DB_INCLUDE_PATH=.)")
 
require s7plc


epicsEnvSet("PREFIX", "CrS-TMCP:Cryo-")
epicsEnvSet("P","CrS-TMCP:")
#IP of PLC
epicsEnvSet("IPADDR", "172.16.114.42")




 




 


######### PLC to IOC
dbLoadRecords(header_sysPLCtoIOC.db, "P=${P}, R=SC-IOC-001:, PLC_NAME=DB1101")
dbLoadRecords(DB1100.db, "PREFIX=${PREFIX}, PLC_NAME=DB1100")
dbLoadRecords(DB1102.db, "PREFIX=${PREFIX}, PLC_NAME=DB1102")
dbLoadRecords(DB1103.db, "PREFIX=${PREFIX}, PLC_NAME=DB1103")
dbLoadRecords(DB1104.db, "PREFIX=${PREFIX}, PLC_NAME=DB1104")
dbLoadRecords(DB1106.db, "PREFIX=${PREFIX}, PLC_NAME=DB1106")
dbLoadRecords(DB1107.db, "PREFIX=${PREFIX}, PLC_NAME=DB1107")
dbLoadRecords(DB1108.db, "PREFIX=${PREFIX}, PLC_NAME=DB1108")
dbLoadRecords(DB1112.db, "PREFIX=${PREFIX}, PLC_NAME=DB1112")
dbLoadRecords(DB1113.db, "PREFIX=${PREFIX}, PLC_NAME=DB1113")
dbLoadRecords(DB1114.db, "PREFIX=${PREFIX}, PLC_NAME=DB1114")
dbLoadRecords(DB1116.db, "PREFIX=${PREFIX}, PLC_NAME=DB1116")
dbLoadRecords(DB1118.db, "PREFIX=${PREFIX}, PLC_NAME=DB1118")


######### IOC to PLC
dbLoadRecords(header_sysIOCtoPLC.db, "P=${P}, R=Cryo-PLC-30000:,PLC_NAME=DB1133, PORT=2012, BYTES=20")
dbLoadRecords(DB1132.db, "PREFIX=${PREFIX}, PLC_NAME=DB1132")
dbLoadRecords(DB1134.db, "PREFIX=${PREFIX}, PLC_NAME=DB1134")
dbLoadRecords(DB1136.db, "PREFIX=${PREFIX}, PLC_NAME=DB1136")
dbLoadRecords(DB1142.db, "PREFIX=${PREFIX}, PLC_NAME=DB1142")
dbLoadRecords(DB1144.db, "PREFIX=${PREFIX}, PLC_NAME=DB1144")
dbLoadRecords(DB1146.db, "PREFIX=${PREFIX}, PLC_NAME=DB1146")
dbLoadRecords(DB1148.db, "PREFIX=${PREFIX}, PLC_NAME=DB1148")


######### Man Auto
dbLoadRecords(manAuto.db, "PREFIX=${PREFIX}")


######### Alarm line
dbLoadRecords(DB1100alarms.db, "PREFIX=${PREFIX}")
dbLoadRecords(DB1102alarms.db, "PREFIX=${PREFIX}")
dbLoadRecords(DB1103alarms.db, "PREFIX=${PREFIX}")
dbLoadRecords(DB1104alarms.db, "PREFIX=${PREFIX}")
dbLoadRecords(DB1107alarms.db, "PREFIX=${PREFIX}")
dbLoadRecords(DB1108alarms.db, "PREFIX=${PREFIX}")
dbLoadRecords(DB1112alarms.db, "PREFIX=${PREFIX}")
dbLoadRecords(DB1113alarms.db, "PREFIX=${PREFIX}")
dbLoadRecords(DB1114alarms.db, "PREFIX=${PREFIX}")
dbLoadRecords(DB1116alarms.db, "PREFIX=${PREFIX}")
dbLoadRecords(DB1118alarms.db, "PREFIX=${PREFIX}")
dbLoadRecords(last_alarm.db, "P=${P}")


######### Load to PLC
dbLoadRecords(load_to_plc.db, "P=${P}")
dbLoadRecords(direct_load.db, "P=${P}, R=SC-IOC-001:")


######### Restore from PLC
dbLoadRecords(restore_from_plc.db, "P=${P}, R=Cryo-PLC-30000:")
dbLoadRecords(rb_to_sp.db, "PREFIX=${PREFIX}, P=${P}")

######### SCREENLOCK
epicsEnvSet("LOCKTIME", "300")
dbLoadRecords(ScreenLock.db, "P=${P}, LOCKTIME=$(LOCKTIME), MAINID=CR")
dbLoadRecords(ScreenLockLocal.db, "P=${P}, LOCKTIME=$(LOCKTIME), LOCKID=ONE")
dbLoadRecords(ScreenLockLocal.db, "P=${P}, LOCKTIME=$(LOCKTIME), LOCKID=TWO")
dbLoadRecords(ScreenLockLocal.db, "P=${P}, LOCKTIME=$(LOCKTIME), LOCKID=LCR1")

#var s7plcDebug 1


######### Configure s7plc
#s7plcConfigure (PLCname, IPaddr, port, inSize[B], outSize[B], bigEndian, recvTimeout[ms], sendInterval[ms])

s7plcConfigure("DB1100", "${IPADDR}", 2030, 2000, 0, 1,  1000,   100)
s7plcConfigure("DB1101", "${IPADDR}", 2011,  100, 0, 1,  1000,   100)
s7plcConfigure("DB1102", "${IPADDR}", 2032, 1700, 0, 1,  1000,   100)
s7plcConfigure("DB1103", "${IPADDR}", 2002,  100, 0, 1,  1000,   100)
s7plcConfigure("DB1104", "${IPADDR}", 2003,  700, 0, 1,  1000,   100)
s7plcConfigure("DB1106", "${IPADDR}", 2004, 2000, 0, 1,  5000,  1000)
s7plcConfigure("DB1107", "${IPADDR}", 2007, 1400, 0, 1,  5000,  1000)
s7plcConfigure("DB1108", "${IPADDR}", 2017,  800, 0, 1,  1000,   100)
s7plcConfigure("DB1112", "${IPADDR}", 2019,  300, 0, 1,  1000,   100)
s7plcConfigure("DB1113", "${IPADDR}", 2001,  200, 0, 1,  1000,   100)
s7plcConfigure("DB1114", "${IPADDR}", 2008, 1100, 0, 1,  5000,  1000)
s7plcConfigure("DB1116", "${IPADDR}", 2013, 1800, 0, 1,  5000,  1000)
s7plcConfigure("DB1118", "${IPADDR}", 2014,  700, 0, 1,  5000,  1000)

s7plcConfigure("DB1132", "${IPADDR}", 2005, 0, 1300, 1,  1000,   100)
s7plcConfigure("DB1133", "${IPADDR}", 2012, 0,  100, 1,  1000,   100)
s7plcConfigure("DB1134", "${IPADDR}", 2006, 0,  200, 1,  1000,   100)
s7plcConfigure("DB1136", "${IPADDR}", 2009, 0, 2000, 1,  5000,  1000)
s7plcConfigure("DB1142", "${IPADDR}", 2021, 0,  200, 1,  1000,   100)
s7plcConfigure("DB1144", "${IPADDR}", 2010, 0,  500, 1,  5000,  1000)
s7plcConfigure("DB1146", "${IPADDR}", 2015, 0, 1500, 1,  5000,  1000)
s7plcConfigure("DB1148", "${IPADDR}", 2016, 0,  500, 1,  5000,  1000)


#asSetFilename("security.acf")

iocInit

#caPutLogInit "127.0.0.1:3050" 1


