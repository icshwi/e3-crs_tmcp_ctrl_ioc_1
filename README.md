# e3-CrS_TMCP_Ctrl_IOC_1

**IMPORTANT !!!!**

This Repo should be used only to hold the **substitutions** files with which the **db** files can be created.

The Repo for deployment is in 

https://gitlab.esss.lu.se/iocs/manual/crs/e3-ioc-crs-tmcp

where the db files should be synchronized with the ones created in this git repo.
